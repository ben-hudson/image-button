package hudson.hupapp;

import java.io.IOException;
import java.io.InputStream;
import java.util.Random;

import javax.microedition.media.Manager;
import javax.microedition.media.MediaException;
import javax.microedition.media.Player;
import javax.microedition.media.control.VolumeControl;

import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Characters;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.Keypad;
import net.rim.device.api.ui.TouchEvent;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.BitmapField;

public class BitmapButtonField extends BitmapField {

	private boolean _active;

	Bitmap down = null;
	Bitmap up = null;

	public BitmapButtonField(Bitmap bitmap) {
		super(bitmap);

		down = new Bitmap(Display.getWidth() - 80, Display.getHeight() - 80);
		up = bitmap;

		Bitmap.getBitmapResource("HupDown.png").scaleInto(down,
				Bitmap.FILTER_LANCZOS, Bitmap.SCALE_TO_FIT);

		down.createAlpha(Bitmap.ALPHA_BITDEPTH_8BPP);
	}

	public void clickButton() {
		fieldChangeNotify(0);
	}

	protected void drawFocus(Graphics g, boolean on) {
	}

	protected boolean invokeAction(int action) {
		switch (action) {
		case ACTION_INVOKE: {
			clickButton();
			return true;
		}
		}

		return super.invokeAction(action);
	}

	public boolean isFocusable() {
		return true;
	}

	protected boolean keyChar(char character, int status, int time) {
		if (character == Characters.ENTER) {
			clickButton();
			return true;
		}

		return super.keyChar(character, status, time);
	}

	protected boolean keyDown(int keycode, int time) {
		if (Keypad.map(Keypad.key(keycode), Keypad.status(keycode)) == Characters.ENTER) {
			if (!_active) {
				playHup();
			}
			_active = true;
			invalidate();
		}

		return super.keyDown(keycode, time);
	}

	protected boolean keyUp(int keycode, int time) {
		if (Keypad.map(Keypad.key(keycode), Keypad.status(keycode)) == Characters.ENTER) {
			_active = false;
			invalidate();
			return true;
		}

		return false;
	}

	protected boolean navigationClick(int status, int time) {
		if (!_active) {
			playHup();
		}
		_active = true;
		invalidate();

		return super.navigationClick(status, time);
	}

	protected boolean navigationUnclick(int status, int time) {
		_active = false;
		invalidate();
		clickButton();

		return true;
	}

	protected void paint(Graphics g) {
		if (_active) {
			setBitmap(down);
		} else {
			setBitmap(up);
		}
		super.paint(g);
	}

	private void playHup() {
		UiApplication.getUiApplication().invokeLater(new Runnable() {
			public void run() {

				Random gen = new Random(System.currentTimeMillis());
				int rand = gen.nextInt(10);
				InputStream is = null;

				switch (rand) {
				case 0:
					is = getClass().getResourceAsStream("/sound/Hup_1.mp3");
					break;
				case 1:
				case 2:
				case 3:
				case 4:
				case 5:
					is = getClass().getResourceAsStream("/sound/Hup_2.mp3");
					break;
				case 6:
				case 7:
					is = getClass().getResourceAsStream("/sound/Hup_3.mp3");
					break;
				case 8:
				case 9:
					is = getClass().getResourceAsStream("/sound/Hup_4.mp3");
					break;
				default:
					is = getClass().getResourceAsStream("/sound/Hup_2.mp3");
					break;
				}

				Player p;
				try {
					p = Manager.createPlayer(is, "audio/mpeg");
					p.realize();
					VolumeControl volume = (VolumeControl) p
							.getControl("VolumeControl");
					volume.setLevel(1000);
					p.prefetch();
					p.start();
				} catch (IOException e) {
					e.printStackTrace();
				} catch (MediaException e) {
					e.printStackTrace();
				}
			}
		});
	}

	protected boolean touchEvent(TouchEvent message) {
		boolean isOutOfBounds = touchEventOutOfBounds(message);
		switch (message.getEvent()) {
		case TouchEvent.DOWN:
			if (!isOutOfBounds) {
				if (!_active) {
					playHup();
					_active = true;
					invalidate();
				}
				return true;
			}
			return false;

		case TouchEvent.UNCLICK:
			if (_active) {
				_active = false;
				invalidate();
			}

			if (!isOutOfBounds) {
				clickButton();
				return true;
			}

		case TouchEvent.UP:
			if (_active) {
				_active = false;
				invalidate();
			}

		default:
			return false;
		}
	}

	private boolean touchEventOutOfBounds(TouchEvent message) {
		int x = message.getX(1);
		int y = message.getY(1);
		return (x < 0 || y < 0 || x > getWidth() || y > getHeight());
	}
}
package hudson.hupapp;

import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.container.AbsoluteFieldManager;
import net.rim.device.api.ui.container.MainScreen;

public final class HomeScreen extends MainScreen {

	public HomeScreen() {
		setTitle((Field) null);

		Bitmap bitmap = Bitmap.getBitmapResource("HupUp.png");
		Bitmap scaled = new Bitmap(Display.getWidth() - 80,
				Display.getHeight() - 80);

		bitmap.scaleInto(scaled, Bitmap.FILTER_LANCZOS, Bitmap.SCALE_TO_FIT);
		scaled.createAlpha(Bitmap.ALPHA_BITDEPTH_8BPP);

		BitmapButtonField bitmapfield = new BitmapButtonField(scaled);

		AbsoluteFieldManager manager = new AbsoluteFieldManager();
		int centerX = (Display.getWidth() - bitmapfield.getBitmapWidth()) / 2;
		int centerY = (Display.getHeight() - bitmapfield.getBitmapHeight()) / 2;

		manager.add(bitmapfield, centerX, centerY);
		add(manager);
	}
}